/**
 * Copyright (c) 2014 Virtue Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.friz;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.friz.applet.GameApplet;
import org.friz.executor.AsynchronousExecutor;
import org.friz.executor.ServerTimeService;
import org.friz.frame.GameFrame;
import org.friz.update.GameUpdate;

/**
 * The main-class of this application.
 * No runtime arguments are needed.
 * 
 * @author Im Frizzy <skype:kfriz1998>
 * @since Dec 28, 2014
 */
public class Misthalin {

	/**
	 * The {@link Misthalin} instance
	 */
	public static Misthalin instance;
	
	private GameApplet applet;
	
	private GameFrame frame;
	
	private GameUpdate update;
	
	private ExecutorService workerPool;
	
	private AsynchronousExecutor workerService;
	
	/**
	 * Entry point of the program. No runtime arguments are needed.
	 * @param args
	 */
	public static void main(String[] args) {
		instance = getInstance();
		try {
			/** First we are going to create new instances */
			
			/* Create a new ExecutorService with 10 threads */
			instance.setWorkerPool(Executors.newFixedThreadPool(10));
			
			/* Create a new AsynchronousExecutor instance */
			instance.setWorkerService(new AsynchronousExecutor());
			
			/* Register events */
			instance.getWorkerService().register(new ServerTimeService());
			
			/* Create a new GameApplet instance */
			instance.setApplet(new GameApplet());
			
			/* Create a new GameFrame instance() */
			instance.setFrame(new GameFrame());
			
			/* Start the executor service */
			instance.getWorkerPool().execute(instance.getWorkerService());
			
			/* Create a new GameUpdate instance & process CRC32 computing */
			instance.setUpdate(new GameUpdate());
			
			/* Process the update procedure if needed */
			instance.getUpdate().process();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public GameApplet getApplet() {
		return applet;
	}
	
	public void setApplet(GameApplet applet) {
		this.applet = applet;
	}
	
	public GameFrame getFrame() {
		return frame;
	}
	
	public void setFrame(GameFrame frame) {
		this.frame = frame;
	}
	
	public GameUpdate getUpdate() {
		return update;
	}
	
	public void setUpdate(GameUpdate update) {
		this.update = update;
	}
	
	/**
	 * @return the workerPool
	 */
	public ExecutorService getWorkerPool() {
		return workerPool;
	}
	
	/**
	 * Sets the worker pool
	 */
	public void setWorkerPool(ExecutorService workerPool) {
		this.workerPool = workerPool;
	}

	/**
	 * Returns the {@link Misthalin} instance
	 */
	public static Misthalin getInstance() {
		if (instance == null) {
			try {
				instance = new Misthalin();
			} catch (Exception e) {
			}
		}
		return instance;
	}

	/**
	 * @return the workerService
	 */
	public AsynchronousExecutor getWorkerService() {
		return workerService;
	}

	/**
	 * @param workerService the workerService to set
	 */
	public void setWorkerService(AsynchronousExecutor workerService) {
		this.workerService = workerService;
	}
}
