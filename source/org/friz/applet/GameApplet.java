/**
 * Copyright (c) 2014 Virtue Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.friz.applet;

import java.applet.Applet;
import java.applet.AppletStub;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Properties;

import javax.swing.JFrame;

import org.friz.Constants;

/**
 * This class is the actual game.
 * @author Im Frizzy <skype:kfriz1998>
 * @since Dec 28, 2014
 */
public class GameApplet extends Applet implements AppletStub{

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 482526999715216786L;
	
	/**
     * Host IP of the Server
     */
    public static String Host = "maxgamer.org";

    /**
     * The parameters of the client.
     */
    private final Properties client_parameters = new Properties();

    /**
     * The current frame of the client application.
     */
    private JFrame clientFrame = null;

    /**
     * Starts the actual client.
     */
    @SuppressWarnings("resource")
    public void startClient() {
        try {
            ClassLoader classLoader = new URLClassLoader(new URL[]{new URL(Constants.JAR_LINK)});

            Class<?> client_class = classLoader.loadClass("client");

            Object v_client = client_class.getConstructor().newInstance();
            client_class.getSuperclass().getMethod("provideLoaderApplet", Applet.class).invoke(v_client, this);
            client_class.getMethod("init").invoke(v_client);
            client_class.getMethod("start").invoke(v_client);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens the actual frame application.
     *
     * @throws MalformedURLException
     */
    public void openFrame() {
        clientFrame = new JFrame("VirtuePK 634");
        clientFrame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:/Users/Fricilone/Desktop/icon.png"));
        clientFrame.add(this);
        clientFrame.setVisible(true);
        clientFrame.setSize(800, 600);
        clientFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Stores the parameters for the client
     */
    public void readParameters() {
        client_parameters.put("cabbase", "g.cab");
        client_parameters.put("java_arguments", "-Xmx1024m -Dsun.java2d.noddraw=true");
        client_parameters.put("colourid", "0");
        client_parameters.put("worldid", "16");
        client_parameters.put("lobbyid", "15");
        client_parameters.put("demoid", "0");
        client_parameters.put("demoaddress", "");
        client_parameters.put("modewhere", "0");
        client_parameters.put("modewhat", "0");
        client_parameters.put("lang", "0");
        client_parameters.put("objecttag", "0");
        client_parameters.put("js", "1");
        client_parameters.put("game", "0");
        client_parameters.put("affid", "0");
        client_parameters.put("advert", "1");
        client_parameters.put("settings", "wwGlrZHF5gJcZl7tf7KSRh0MZLhiU0gI0xDX6DwZ-Qk");
        client_parameters.put("country", "0");
        client_parameters.put("haveie6", "0");
        client_parameters.put("havefirefox", "1");
        client_parameters.put("cookieprefix", "");
        client_parameters.put("cookiehost", "maxgamer.org");
        client_parameters.put("cachesubdirid", "0");
        client_parameters.put("crashurl", "");
        client_parameters.put("unsignedurl", "");
        client_parameters.put("sitesettings_member", "1");
        client_parameters.put("frombilling", "false");
        client_parameters.put("sskey", "");
        client_parameters.put("force64mb", "false");
        client_parameters.put("worldflags", "8");
        client_parameters.put("lobbyaddress", Host);
    }

    /*
     * (non-Javadoc)
     * @see java.applet.AppletStub#appletResize(int, int)
     */
    @Override
    public void appletResize(int dimensionX, int dimensionY) {
        super.resize(new Dimension(dimensionX, dimensionY));
    }

    /*
     * (non-Javadoc)
     * @see java.applet.Applet#getParameter(java.lang.String)
     */
    @Override
    public String getParameter(String paramName) {
        return (String) client_parameters.get(paramName);
    }

    /*
     * (non-Javadoc)
     * @see java.applet.Applet#getDocumentBase()
     */
    @Override
    public URL getDocumentBase() {
        try {
            return new URL("http://" + Host);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * @see java.applet.Applet#getCodeBase()
     */
    @Override
    public URL getCodeBase() {
        try {
            return new URL("http://" + Host);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
