/**
 * Copyright (c) 2014 Virtue Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.friz.update;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.jar.JarFile;
import java.util.zip.CRC32;

import javax.swing.JOptionPane;

import org.friz.Constants;
import org.friz.Misthalin;

/**
 * This class handles updating the loader.
 * Updating will be determined by computing {@code CRC32} Checksum
 * 
 * @author Im Frizzy <skype:kfriz1998>
 * @since Dec 28, 2014
 */
public class GameUpdate {
	
	/**
	 * CRC32 Checksum of the local jar
	 */
	private long local;
	
	/**
	 * CRC32 Checksum of the latest jar (on website)
	 */
	private long latest;
	
	public GameUpdate() {
		CRC32 crc = new CRC32();
		crc.reset();

		int pointer;
		byte[] buffer = new byte[1024];
		InputStream stream;
		
        Misthalin.getInstance().getFrame().getProgressLabel().setVisible(true);
        Misthalin.getInstance().getFrame().getProgressLabel().setBounds(220, 380, 200, 16);
		Misthalin.getInstance().getFrame().getProgressLabel().setText("Currently Checking for Updates.");
		
		try {
			//System.out.println(new JarFile(new File(GameUpdate.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())).getManifest().getMainAttributes().getValue("Bundle-Version"));
		//	Misthalin.getInstance().getFrame().getLocalVersion().setText(new JarFile(new File(GameUpdate.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath())).getManifest().getMainAttributes().getValue("Bundle-Version"));
			stream = new FileInputStream(new File("C:/Users/Kyle Friz/Desktop/Misthalin.jar"));//GameUpdate.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
			while ((pointer = stream.read(buffer)) > 0) {
				crc.update(buffer, 0, pointer);
			}
			
			stream.close();
			local = crc.getValue();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		buffer = new byte[1024];
		crc.reset();
		
		try {
		//	Misthalin.getInstance().getFrame().getLatestVersion().setText(new JarFile(new File(new URL(Constants.JAR_LINK).toURI())).getManifest().getMainAttributes().getValue("Bundle-Version"));
			stream = new URL(Constants.JAR_LINK).openStream();
			while ((pointer = stream.read(buffer)) > 0) {
				crc.update(buffer, 0, pointer);
			}
			
			stream.close();
			latest = crc.getValue();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Processes the update
	 */
	public void process() {
		//if (local != latest) {
			try {
				int size = -1;
				int read = 0;
				
				// Open connection to URL.
		        HttpURLConnection connection =  (HttpURLConnection) new URL(Constants.JAR_LINK).openConnection();

		        // Specify what portion of file to download.
		        connection.setRequestProperty("Range", "bytes=" + read + "-");

		        // Connect to server.
		        connection.connect();

		        // Make sure response code is in the 200 range.
		        if (connection.getResponseCode() / 100 != 2) {
		        	Misthalin.getInstance().getFrame().getProgressLabel().setText("Response Code Unexpected.");
		        	return;
		        }

		        // Check for valid content length.
		        int contentLength = connection.getContentLength();
		        if (contentLength < 1) {
		        	Misthalin.getInstance().getFrame().getProgressLabel().setText("Invalid Content Length.");
		        	return;
		        }

		        /* Set the size for this download if it hasn't been already set. */
		        if (size == -1) {
		            size = contentLength;
		        }
				
				FileOutputStream outStream = new FileOutputStream(new File("C:/Users/Kyle Friz/Desktop/Misthalin.jar"));//GameUpdate.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()));
		        InputStream inStream = connection.getInputStream();
				
		        
		        Misthalin.getInstance().getFrame().getProgressBar().setVisible(true);
		        Misthalin.getInstance().getFrame().getProgressBarPointer().setVisible(true);
		        Misthalin.getInstance().getFrame().getProgressLabel().setBounds(250, 380, 200, 16);
		        
				int pointer;
				float percent;
				DecimalFormat format = new DecimalFormat("###");
				byte[] buffer = new byte[1024];
				while ((pointer = inStream.read(buffer)) > 0) {
				    outStream.write(buffer, 0, pointer);
					read += pointer;
					percent = read / size;
					Misthalin.getInstance().getFrame().getProgressBar().setBounds(4, 397, (int) (630 * percent), 16);
					Misthalin.getInstance().getFrame().getProgressBarPointer().setBounds((int) (630 * percent) - 5, 397, 10, 16);
					Misthalin.getInstance().getFrame().getProgressLabel().setText("Currently Patching: " + format.format(percent * 100) + "%");
				}
			
				Misthalin.getInstance().getFrame().getProgressLabel().setText("Currently Restarting.");
				
				connection.disconnect();
				outStream.close();
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, "Misthalin has been updated. It will now restart", "Misthalin", JOptionPane.INFORMATION_MESSAGE);
		//} else {
			
		//}
	}
}
